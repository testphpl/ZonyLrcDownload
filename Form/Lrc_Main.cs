﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Collections;
using ID3;

namespace Zony_Lrc_Download_2._0
{
    public partial class Lrc_Main : Form
    {
        public Lrc_Main()
        {
            InitializeComponent();
        }
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://jq.qq.com/?_wv=1027&k=Zrl68q");   
        }
        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            About s1 = new About();
            s1.Show();
        }

        private void Button_SelectDirectory_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folder_Lrc = new FolderBrowserDialog();

            folder_Lrc.Description = "请选择歌曲所在的文件夹：";
            folder_Lrc.ShowDialog();
            LrcPath = folder_Lrc.SelectedPath;
            
            if(LrcPath == "")
            {
                MessageBox.Show(null,"请选择正确的文件夹路径！","提示",MessageBoxButtons.OK,MessageBoxIcon.Information);
                #region 日志点
                Log.WriteLog("文件夹选择错误。\n");
                #endregion
            }
            else
            {
                toolStripStatusLabel1.Text = "正在扫描......";
                Button_SelectDirectory.Enabled = false;
                #region 日志点
                Log.WriteLog("开始扫描MP3文件。\n");
                #endregion

                Thread Search = new Thread(SearchFile);
                Search.Start();
                #region 日志点
                Log.WriteLog("扫描线程启动，线程ID："+Search.ManagedThreadId.ToString()+"\n");
                #endregion
            }

        }
        private void 关于ToolStripMenuItem_Click(object sender, EventArgs e)
       {
                About s1 = new About();
                s1.Show();
       }
        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
       {
           if (WindowState == FormWindowState.Minimized)
           {
               //还原窗体显示 
               WindowState = FormWindowState.Normal;
               //激活窗体并给予它焦点 
               this.Activate();
               //任务栏区显示图标 
               this.ShowInTaskbar = true;
           }
       }
        private void Lrc_Main_Load(object sender, EventArgs e)
        {
            // 允许非安全线程代码
            Control.CheckForIllegalCrossThreadCalls = false;
            // 加载图标
            this.Icon = Zony_Lrc_Download_2._0.Resource1._6;
        }
        private void Lrc_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            // 销毁托盘图标
            notifyIcon1.Dispose();
            Environment.Exit(0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "下载歌词......";
            Thread Down = new Thread(DownLoadLrc);
            Down.Start();
            #region 日志点
            Log.WriteLog("歌词下载线程启动，线程ID：" + Down.ManagedThreadId.ToString() + "\n");
            #endregion
        }

        private void LrcListItem_Click(object sender, EventArgs e)
        {
            if(LrcListItem.SelectedItems.Count>0)
            {
                try
                {
                    label5.Text = "歌曲路径:" + (string)m_lrcPath[LrcListItem.SelectedItems[0].Index];
                    ID3Info id3 = new ID3Info((string)m_lrcPath[LrcListItem.SelectedItems[0].Index], true);

                    label2.Text = id3.ID3v1Info.Title != "" ? "歌曲名称:" + id3.ID3v1Info.Title : "歌曲名称:" + id3.ID3v2Info.GetTextFrame("TIT2");
                    label1.Text = id3.ID3v1Info.Artist != "" ? "歌手:" + id3.ID3v1Info.Artist : "歌手:" + id3.ID3v2Info.GetTextFrame("TPE1");

                }
                catch(System.OverflowException)
                {
                    label2.Text = "歌曲名称:" + "none";
                    label1.Text = "歌手:" + "none";
                }
            }
        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 销毁托盘图标
            notifyIcon1.Dispose();
            Environment.Exit(0);
        }

        private void Lrc_Main_SizeChanged(object sender, EventArgs e)
        {
            //判断是否选择的是最小化按钮 
            if (WindowState == FormWindowState.Minimized)
            {
                //隐藏任务栏区图标 
                this.ShowInTaskbar = false;
            }
        }

        private void 显示主窗口ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                //还原窗体显示 
                WindowState = FormWindowState.Normal;
                //激活窗体并给予它焦点 
                this.Activate();
                //任务栏区显示图标 
                this.ShowInTaskbar = true;
            }
        }
    }
}
